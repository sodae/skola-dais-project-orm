﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Model
{
    public class Map
    {
        public delegate object DFieldCallback(object entity, Dictionary<string, object> row);

        public enum Type
        {
            INT,
            STRING,
            BOOL,
            DATETIME,
            ENTITY,
            AUTO,
            CALLBACK
        };

        private class MetaField
        {
            public MetaEntity Entity;
            public string Field;
            public string Attribute;
            public Type Datatype;
            public DFieldCallback Callback;
        }

        private class MetaEntity
        {
            public System.Type ClassName;
            public string Alias;
            public List<MetaField> Attributes;
        }


        private Dictionary<string, MetaEntity> entity;
        private Dictionary<string, MetaField> fields;

        private string top;

        public Map()
        {
            this.entity = new Dictionary<string, MetaEntity>();
            this.fields = new Dictionary<string, MetaField>();
        }

        public Map(string alias, System.Type className)
            : this()
        {
            this.AddEntity(alias, className);
            this.SetTop(alias);
        }

        public void SetTop(string aliasEntity)
        {
            if (!this.entity.ContainsKey(aliasEntity))
            {
                throw new Exception("Unknown entity");
            }
            this.top = aliasEntity;
        }

        public void AddEntity(string alias, System.Type className)
        {
            if (className == null)
            {
                throw new ArgumentNullException();
            }

            this.entity.Add(alias, new MetaEntity()
            {
                Alias = alias,
                ClassName = className,
                Attributes = new List<MetaField>()
            });
        }

        public void AddFieldEntity(string entityAlias, string targetEntityAlias, string targetAttribute)
        {
            MetaField ms = new MetaField()
            {
                Entity = this.entity[targetEntityAlias],
                Field = entityAlias,
                Attribute = targetAttribute,
                Datatype = Type.ENTITY
            };

            this.entity[targetEntityAlias].Attributes.Add(ms);
        }

        public void AddFieldScalar(string field, string entityAlias, string attribute, Type dataType = Type.AUTO)
        {
            MetaField ms = new MetaField()
            {
                Entity = this.entity[entityAlias],
                Field = field,
                Attribute = attribute,
                Datatype = dataType
            };

            this.fields.Add(field, ms);
            this.entity[entityAlias].Attributes.Add(ms);
        }

        public void AddFieldCallback(string entityAlias, string attribute, DFieldCallback callback)
        {
            MetaField ms = new MetaField()
            {
                Entity = this.entity[entityAlias],
                Attribute = attribute,
                Datatype = Type.CALLBACK,
                Callback = callback
            };

            this.entity[entityAlias].Attributes.Add(ms);
        }

        public List<T> Mapping<T>(SqlDataReader result)
        {
            List<T> collection = new List<T>();
            while (result.Read())
            {
                Dictionary<string, object> createdEntity = new Dictionary<string, object>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                for (int lp = 0; lp < result.FieldCount; lp++)
                {
                    row.Add(result.GetName(lp), result.GetValue(lp)); // for callback

                    if (!this.fields.ContainsKey(result.GetName(lp))) { continue; }

                    MetaField fieldMetadata = this.fields[result.GetName(lp)];
                    MetaEntity entityMetadata = fieldMetadata.Entity;

                    Object entity = null;
                    if (!createdEntity.TryGetValue(entityMetadata.Alias, out entity))
                    {
                        entity = Activator.CreateInstance(entityMetadata.ClassName);
                        createdEntity.Add(entityMetadata.Alias, entity);
                    }

                    this.setPropValue(entityMetadata.ClassName, entity, fieldMetadata.Attribute, this.convertObject(fieldMetadata, result, lp));
                }

                foreach (KeyValuePair<string, object> keyValue in createdEntity)
                {
                    foreach (MetaField field in this.entity[keyValue.Key].Attributes)
                    {
                        if (field.Datatype == Type.ENTITY)
                        {
                            setPropValue(keyValue.Value.GetType(), keyValue.Value, field.Attribute, createdEntity[field.Field]);
                        }
                        else if (field.Datatype == Type.CALLBACK)
                        {
                            setPropValue(keyValue.Value.GetType(), keyValue.Value, field.Attribute, field.Callback(keyValue.Value, row));
                        }
                    }
                }

                collection.Add((T)createdEntity[this.top]);
            }

            result.Close();

            return collection;
        }

        private void setPropValue(System.Type type, object obj, string propName, object value)
        {
            var prop = type.GetProperty(propName);
            if (prop != null)
            {
                prop.SetValue(obj, value, null);
                return;
            }

            var field = type.GetField(propName);
            if (field != null)
            {
                field.SetValue(obj, value);
                return;
            }

            throw new ArgumentException("Unknown property/field name");
        }

        private object convertObject(MetaField field, SqlDataReader result, int i)
        {
            var x = result.GetValue(i);
            if (result.IsDBNull(i)) return null;
            if (field.Datatype == Type.AUTO) return result.GetValue(i);
            if (field.Datatype == Type.INT) return result.GetInt32(i);
            if (field.Datatype == Type.BOOL) return (byte) result.GetValue(i) > 0;
            if (field.Datatype == Type.STRING) return result.GetString(i);
            if (field.Datatype == Type.DATETIME) return result.GetDateTime(i);

            throw new Exception("Invalid state");
        }
    }
}
