﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Model
{
    public class Connection
    {
        private SqlConnection connection;
        private SqlTransaction transaction = null;

        private static string DEFAULT_CONNECTION_STRING =
            @"Data Source=SODAE-V3\SQLEXPRESS;Initial Catalog=dais_2;Integrated Security=SSPI;MultipleActiveResultSets=true;connection timeout=3";

        public Connection(string connectionString = null)
        {
            this.connection = new SqlConnection(connectionString ?? DEFAULT_CONNECTION_STRING);
        }

        public void Open()
        {
            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
                if (connection.State != System.Data.ConnectionState.Open)
                {
                    throw new NoConnectedException();
                }
            }
        }

        public void Close()
        {
            connection.Close();
        }


        /***** Transaction *****/

        public void BeginTransaction()
        {
            this.transaction = connection.BeginTransaction(IsolationLevel.Serializable);
        }

        public void EndTransaction()
        {
            this.transaction.Commit();
        }

        public void Rollback()
        {
            transaction.Rollback();
        }

        public void Dispose()
        {
            this.EndTransaction();
        }


        /**
         * Execute query (with set connection and transaction) and return result in SqlDataReader
         */
        public SqlDataReader Fetch(SqlCommand command)
        {
            command.Connection = this.connection;
            command.Transaction = this.transaction;
            command.Prepare();

            SqlDataReader sqlReader = command.ExecuteReader();
            return sqlReader;
        }


        /**
         * Execute query (with set connection and transaction) and return scalar
         */
        public Object FetchScalar(SqlCommand command)
        {
            command.Connection = this.connection;
            command.Transaction = this.transaction;
            command.Prepare();

            return command.ExecuteScalar();
        }



        /**
         * Execute command (with set connection and transaction) and return count of modified rows
         */
        public int ExecuteNonQuery(SqlCommand command)
        {
            command.Connection = this.connection;
            command.Transaction = this.transaction;

            int rowNumber = 0;
            command.Prepare();
            rowNumber = command.ExecuteNonQuery();
            return rowNumber;
        }
    }


    public class NoConnectedException : Exception {}
}
