﻿namespace Model
{
    public class BaseRepository
    {
        protected Connection Connection;

        public BaseRepository(Connection connection)
        {
            this.Connection = connection;
        }

    }
}
