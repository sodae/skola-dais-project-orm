﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Collection<T>
    {
        private DLazyLoad loadCallback;

        protected List<T> List { get; private set; }

        public int Count
        {
            get
            {
                this.loadData();
                return this.List.Count;
            }
        }

        public Collection()
        {
        }

        public Collection(List<T> list)
        {
            this.List = list;
        }

        public Collection(DLazyLoad loadCallback)
        {
            this.loadCallback = loadCallback;
        }

        private void loadData()
        {
            if (this.List == null)
            {
                this.List = this.loadCallback != null ? this.loadCallback() : new List<T>();
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            this.loadData();
            return this.List.GetEnumerator();
        }


        public delegate List<T> DLazyLoad();
    }

}
