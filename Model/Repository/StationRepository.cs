﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Model.Entity;
using Model.Entity.Service;

namespace Model.Repository
{
    public class StationRepository : BaseRepository
    {

        public StationRepository(Connection connection) : base(connection)
        {
        }

        public Station GetStation(string stationCode)
        {
            SqlCommand command = new SqlCommand(
                "SELECT * FROM station AS s " +
                "WHERE s.code = @code"
                );
            command.Parameters.Add("@code", SqlDbType.NVarChar, 10).Value = stationCode;

            List<Station> list = this.createMapStation().Mapping<Station>(Connection.Fetch(command));
            if (list.Count > 1)
               throw new Exception("Too many rows");
            if (list.Count == 1)
                return list[0];
            return null;
        }

        public Collection<Station> FindService()
        {
            SqlCommand command = new SqlCommand("SELECT * FROM train_service AS s ");
            return new Collection<Station>(() => this.createMapStation().Mapping<Station>(Connection.Fetch(command)));
        }

        private Map createMapStation()
        {
            Map map = new Map();
            map.AddEntity("station", Type.GetType("Model.Entity.Station"));
            map.AddFieldScalar("code", "service", "Code");
            map.AddFieldScalar("name", "service", "Name");

            map.SetTop("station");

            return map;
        }

    }
}