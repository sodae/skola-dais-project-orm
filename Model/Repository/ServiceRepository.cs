﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Model.Entity.Reservation;
using Model.Entity.Service;

namespace Model.Repository
{
    public class ServiceRepository : BaseRepository
    {

        public ServiceRepository(Connection connection) : base(connection)
        {
        }

        public Service GetService(int serviceId)
        {
            SqlCommand command = new SqlCommand(
                "SELECT * FROM train_service AS s " +
                "WHERE s.id = @id"
                );
            command.Parameters.Add("@id", SqlDbType.Int).Value = serviceId;

            List<Service> list = this.createMapService().Mapping<Service>(Connection.Fetch(command));
            if (list.Count > 1)
               throw new Exception("Too many rows");
            if (list.Count == 1)
                return list[0];
            return null;
        }

        public Collection<Service> FindService()
        {
            SqlCommand command = new SqlCommand("SELECT * FROM train_service AS s ");
            return new Collection<Service>(() => this.createMapService().Mapping<Service>(Connection.Fetch(command)));
        }

        // Analýza 1
        public Collection<SearchServiceResult> FindServiceByStation(string stationCodeStart, string stationCodeEnd, bool onRail = false, DateTime? time = null)
        {
            if (stationCodeStart == null || stationCodeEnd == null || stationCodeStart == "" || stationCodeEnd == "") 
                throw new Exception();

            SqlCommand command = new SqlCommand(
                "SELECT s.*, ta.departure, td.arrival FROM train_service s " +
                "LEFT JOIN (SELECT MIN(t.arrival) as min_arrival, t.service_id FROM timetable t GROUP BY t.service_id) " +
                "   AS a ON s.id = a.service_id " +
                "LEFT JOIN (SELECT MAX(t.arrival) as max_arrival, t.service_id FROM timetable t GROUP BY t.service_id) " +
                "   AS b ON s.id = b.service_id " +
                "INNER JOIN timetable ta ON s.id = ta.service_id AND ta.station_code = @startStation " +
                "INNER JOIN timetable td ON s.id = td.service_id AND td.station_code = @endStation " +
                "WHERE " +
                (onRail ? "a.min_arrival < @time AND b.max_arrival > @time " : "a.min_arrival > @time ") +
                "   AND ta.departure IS NOT NULL " +
                "   AND ta.departure < td.arrival"
            );
            command.Parameters.Add("@startStation", SqlDbType.NChar, 10).Value = stationCodeStart;
            command.Parameters.Add("@endStation", SqlDbType.NChar, 10).Value = stationCodeEnd;
            command.Parameters.Add("@time", SqlDbType.DateTime2, 255).Value = time ?? System.DateTime.Now;

            Map map = this.createMapService();
            map.AddEntity("result", Type.GetType("Model.Entity.Reservation.SearchServiceResult"));
            map.AddFieldScalar("arrival", "result", "Arrival");
            map.AddFieldScalar("departure", "result", "Departure");
            map.AddFieldEntity("service", "result", "Service");
            map.SetTop("result");

            return new Collection<SearchServiceResult>(() => map.Mapping<SearchServiceResult>(Connection.Fetch(command)));
        }

        private Map createMapService()
        {
            Map map = new Map();
            map.AddEntity("service", Type.GetType("Model.Entity.Service.Service"));
            map.AddFieldScalar("id", "service", "Id");
            map.AddFieldScalar("code", "service", "Code");
            map.AddFieldScalar("type", "service", "Type");
            map.AddFieldCallback("service", "Timetable", (object entity, Dictionary<string, object> row) =>
            {
                return this.FindServiceTimetable( ((Service)entity).Id );
            });

            map.SetTop("service");

            return map;
        }

        public Collection<Seat> FindSeatByService(int serviceId)
        {
            SqlCommand command = new SqlCommand(
                "SELECT ws.*, rp.reservation_code, rp.price FROM wagon_seat ws " +
                "LEFT JOIN reservation_place rp " +
                "   ON rp.seat_number = ws.seat_number " +
                "   AND rp.service_id = ws.wagon_service_id " +
                "   AND rp.wagon_order = ws.wagon_order " +
                "WHERE ws.wagon_service_id = @id");
            command.Parameters.Add("@id", SqlDbType.Int).Value = serviceId;

            Map map = new Map();
            map.AddEntity("service", Type.GetType("Model.Entity.Service.Service"));
            map.AddEntity("seat", Type.GetType("Model.Entity.Service.Seat"));
            map.AddEntity("wagon", Type.GetType("Model.Entity.Service.Wagon"));
            map.AddEntity("place", Type.GetType("Model.Entity.Reservation.Place"));
            map.AddEntity("reservation", Type.GetType("Model.Entity.Reservation.Reservation"));

            map.AddFieldScalar("wagon_service_id", "service", "Id");
            map.AddFieldScalar("wagon_order", "wagon", "Order");
            map.AddFieldScalar("seat_number", "seat", "SeatNumber");
            map.AddFieldScalar("type", "seat", "Type");
            map.AddFieldScalar("price", "place", "Price");
            map.AddFieldScalar("reservation_code", "reservation", "Code");

            map.AddFieldEntity("place", "seat", "Reservation");
            map.AddFieldEntity("reservation", "place", "Reservation");
            map.AddFieldEntity("wagon", "seat", "Wagon");
            map.AddFieldEntity("service", "wagon", "Service");

            map.SetTop("seat");

            return new Collection<Seat>(() => map.Mapping<Seat>(Connection.Fetch(command)));
        }

        public Collection<Timetable> FindServiceTimetable(int serviceId)
        {
            SqlCommand command = new SqlCommand(
                "SELECT * FROM timetable AS t LEFT JOIN station AS s ON t.station_code = s.code " +
                "WHERE t.service_id = @id " +
                "ORDER BY t.arrival ASC"
            );
            command.Parameters.Add("@id", SqlDbType.Int).Value = serviceId;

            Map map = new Map();
            map.AddEntity("timetable", Type.GetType("Model.Entity.Service.Timetable"));
            map.AddFieldScalar("arrival", "timetable", "Arrival");
            map.AddFieldScalar("departure", "timetable", "Departure", Map.Type.DATETIME);

            map.AddEntity("station", Type.GetType("Model.Entity.Station"));
            map.AddFieldScalar("name", "station", "Name");
            map.AddFieldScalar("code", "station", "Code");

            map.AddFieldEntity("station", "timetable", "Station");
            map.SetTop("timetable");

            return new Collection<Timetable>(() => map.Mapping<Timetable>(Connection.Fetch(command)));
        }

        public void ExtendService(int serviceId, int size, string type = "Economy")
        {
            Connection.BeginTransaction();
            try
            {
                SqlCommand command = new SqlCommand("service_extend");
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@service_id", SqlDbType.Int).Value = serviceId;
                command.Parameters.Add("@size", SqlDbType.Int).Value = size;
                command.Parameters.Add("@type", SqlDbType.NVarChar, 10).Value = type;
                this.Connection.ExecuteNonQuery(command);
            }
            catch (Exception e)
            {
                Connection.Rollback();
                throw;
            }
            finally
            {
                Connection.EndTransaction();
            }
        }

        public void CloneService(int serviceId, DateTime firstArrival)
        {
            Connection.BeginTransaction();
            try
            {
                SqlCommand command = new SqlCommand("clone_service");
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@service_id", SqlDbType.Int).Value = serviceId;
                command.Parameters.Add("@newtime", SqlDbType.DateTime2).Value = firstArrival;
                this.Connection.ExecuteNonQuery(command);
            }
            catch (Exception e)
            {
                Connection.Rollback();
                throw;
            }
            finally
            {
                Connection.EndTransaction();
            }
        }

    }
}
