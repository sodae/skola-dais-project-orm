﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Model.Entity;

namespace Model.Repository
{
    public class TrainRepository: BaseRepository
    {
        public TrainRepository(Connection connection) : base(connection)
        {
        }

        public Collection<Train> FindTrain()
        {
            var result = this.Connection.Fetch(new SqlCommand(
                "SELECT ed.id as ed_id, ed.name as ed_name, t.code as t_code, t.title as t_title " +
                "FROM train t " +
                "LEFT JOIN engine_driver as ed ON ed.id = t.engine_driver_id"
            ));

            Map map = this.createMapperEnginerTrain(true);
            return new Collection<Train>(() => map.Mapping<Train>(result));
        }

        public Collection<EngineDriver> FindEnginer()
        {
            var result = this.Connection.Fetch(new SqlCommand(
                "SELECT ed.id as ed_id, ed.name as ed_name, t.code as t_code, t.title as t_title " +
                "FROM engine_driver as ed " +
                "LEFT JOIN train t ON ed.id = t.engine_driver_id"
            ));

            Map map = this.createMapperEnginerTrain(false);
            return new Collection<EngineDriver>(() => map.Mapping<EngineDriver>(result));
        }

        private Map createMapperEnginerTrain(bool trainIsMain)
        {
            Map map = new Map();
            map.AddEntity("enginer", Type.GetType("Model.Entity.EngineDriver"));
            map.AddEntity("train", Type.GetType("Model.Entity.Train"));

            map.SetTop(trainIsMain ? "train" : "enginer");

            map.AddFieldScalar("ed_id", "enginer", "Id");
            map.AddFieldScalar("ed_name", "enginer", "Name");

            map.AddFieldScalar("t_code", "train", "Code");
            map.AddFieldScalar("t_title", "train", "Title");

            map.AddFieldEntity("enginer", "train", "EngineDriver");
            map.AddFieldEntity("train", "enginer", "Train");

            return map;

        }

    }
}
