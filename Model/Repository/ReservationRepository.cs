﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.Eventing.Reader;
using System.IO.MemoryMappedFiles;
using System.Security.AccessControl;
using Model.Entity.Reservation;
using Model.Entity.Service;

namespace Model.Repository
{
    public class ReservationRepository : BaseRepository
    {
        public ReservationRepository(Connection connection) : base(connection)
        {
        }

        public Collection<Reservation> FindReservation()
        {
            SqlCommand command = new SqlCommand("SELECT * FROM reservation AS r ");
            return new Collection<Reservation>(() => this.createMapReservation().Mapping<Reservation>(Connection.Fetch(command)));
        }

        public Reservation GetReservation(string code)
        {
            if (string.IsNullOrEmpty(code)) return null;

            SqlCommand command = new SqlCommand(
                "SELECT * FROM reservation AS r " +
                "WHERE r.code = @code"
                );
            command.Parameters.Add("@code", SqlDbType.NVarChar, 200).Value = code;

            List<Reservation> list = this.createMapReservation().Mapping<Reservation>(Connection.Fetch(command));
            if (list.Count > 1)
                throw new Exception("Too many rows");
            if (list.Count == 1)
                return list[0];
            return null;
        }


        private Map createMapReservation()
        {
            Map map = new Map();
            map.AddEntity("r", Type.GetType("Model.Entity.Reservation.Reservation"));
            map.AddFieldScalar("code", "r", "Code");
            map.AddFieldScalar("name", "r", "Name");
            map.AddFieldScalar("email", "r", "Email");
            map.AddFieldScalar("paid", "r", "Paid", Map.Type.BOOL);
            map.AddFieldScalar("created", "r", "Created");
            map.SetTop("r");

            map.AddFieldCallback("r", "SimplifiedPlaces", (object entity, Dictionary<string, object> row) => this.FindReservationSimplifiedPlaces((string) row["code"]));
            return map;
        }

        public Collection<SimplifiedPlace> FindReservationSimplifiedPlaces(string code)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM reservation_place AS place WHERE reservation_code = @code");
            command.Parameters.Add("@code", SqlDbType.NVarChar, 200).Value = code;

            Map map = new Map();
            map.AddEntity("e", Type.GetType("Model.Entity.Reservation.SimplifiedPlace"));
            map.AddFieldScalar("wagon_order", "e", "WagonOrder");
            map.AddFieldScalar("seat_number", "e", "SeatNumber");
            map.AddFieldScalar("service_id", "e", "ServiceId");
            map.SetTop("e");

            return new Collection<SimplifiedPlace>(() => map.Mapping<SimplifiedPlace>(Connection.Fetch(command)));
        }

        public string CreateReservation(string name, string email, List<SimplifiedPlace> seats)
        {
            Reservation reservation = new Reservation();
            reservation.Name = name;
            reservation.Email = email;
            reservation.Paid = false;
            reservation.Created = System.DateTime.Now;

            Connection.BeginTransaction();

            try
            {
                SqlCommand cmd = new SqlCommand("create_reservation");
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@name", SqlDbType.NVarChar, 100).Value = reservation.Name;
                cmd.Parameters.Add("@email", SqlDbType.NVarChar, 200).Value = reservation.Email;
                cmd.Parameters.Add("@paid", SqlDbType.Int).Value = reservation.Paid ? 1 : 0;
                cmd.Parameters.Add(new SqlParameter("@code", SqlDbType.NVarChar, 40));
                cmd.Parameters["@code"].Direction = ParameterDirection.Output;

                Connection.ExecuteNonQuery(cmd);
                reservation.Code = (string)cmd.Parameters["@code"].Value;

                foreach (var place in seats)
                {
                    SqlCommand command3 = new SqlCommand(
                        "INSERT INTO reservation_place (reservation_code, wagon_order, seat_number, service_id, status, price) " +
                        "VALUES (@code, @order, @number, @service, @status, @price)");
                    command3.Parameters.Add("@code", SqlDbType.NVarChar, 40).Value = reservation.Code;
                    command3.Parameters.Add("@order", SqlDbType.NVarChar, 100).Value = place.WagonOrder;
                    command3.Parameters.Add("@number", SqlDbType.NVarChar, 200).Value = place.SeatNumber;
                    command3.Parameters.Add("@service", SqlDbType.Int).Value = place.ServiceId;
                    command3.Parameters.Add("@status", SqlDbType.Int).Value = 0;
                    command3.Parameters.Add("@price", SqlDbType.Int).Value = 100;
                    Connection.ExecuteNonQuery(command3);
                }
                Connection.EndTransaction();
                return reservation.Code;
 
            }
            catch (Exception)
            {
                Connection.Rollback();
                throw;
            }

        }


        public void updateReservation(Reservation reservation)
        {
            SqlCommand command = new SqlCommand(
                    "UPDATE reservation SET name = @name, email = @email, paid = @paid WHERE code = @code");
            command.Parameters.Add("@code", SqlDbType.NVarChar, 40).Value = reservation.Code;
            command.Parameters.Add("@name", SqlDbType.NVarChar, 100).Value = reservation.Name;
            command.Parameters.Add("@email", SqlDbType.NVarChar, 200).Value = reservation.Email;
            command.Parameters.Add("@paid", SqlDbType.Int).Value = reservation.Paid ? 1 : 0;
            Connection.ExecuteNonQuery(command);
        }


        public void cancelReservation(string code, int serviceId = 0)
        {
            Connection.BeginTransaction();

            Reservation reservation = this.GetReservation(code);
            if (reservation == null) return; // not found, so canceled

            try
            {
                SqlCommand command = new SqlCommand("cancel_reservation");
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@code_reservation", SqlDbType.NVarChar, 200).Value = code;
                command.Parameters.Add("@service_id", SqlDbType.Int).Value = serviceId;
                this.Connection.ExecuteNonQuery(command);
            }
            catch (Exception e)
            {
                Connection.Rollback();
                throw;
            }
            finally
            {
                Connection.EndTransaction();
            }
            

        }
    }
}