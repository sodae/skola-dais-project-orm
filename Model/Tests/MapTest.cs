﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Model.Tests
{
    public class MapTest
    {

        public static void MappingTest()
        {
            Connection connection = new Connection();
            connection.Open();
            var result = connection.Fetch(new SqlCommand("SELECT 123 as a, 'hello!' as b, 1 as c, SYSDATETIME() as d"));

            Map map = new Map("my", Type.GetType("Model.Tests.MyEntity", true));
            map.AddFieldScalar("a", "my", "my_num", Map.Type.INT);
            map.AddFieldScalar("b", "my", "my_string", Map.Type.STRING);

            map.AddEntity("second", Type.GetType("Model.Tests.MyEntity", true));
            map.AddFieldScalar("c", "second", "my_bool", Map.Type.BOOL);

            map.AddEntity("something", Type.GetType("Model.Tests.OtherClass", true));
            map.AddFieldScalar("d", "something", "my_datetime", Map.Type.DATETIME);

            map.AddFieldEntity("second", "my", "my_second");
            map.AddFieldEntity("my", "second", "my_second");
            map.AddFieldEntity("something", "second", "lol");

            map.AddFieldCallback("something", "random_data", (object entity, Dictionary<string, object> row) =>
            {
                return "AHOJ!" + row["b"];
            });

            //Mapper mapper = new Mapper();
            List<MyEntity> list = map.Mapping<MyEntity>(result);

            if (list.Count != 1) throw new Exception();
            if (list[0].my_second == null) throw new Exception();
            if (list[0].my_second == list[0]) throw new Exception();
            if (list[0].my_second.lol == null) throw new Exception();
            if (list[0].my_second.my_second != list[0]) throw new Exception();
            if (list[0].my_second.my_bool == false) throw new Exception();
            if (list[0].my_num != 123) throw new Exception();
            if (list[0].lol != null) throw new Exception();
            if (list[0].my_string != "hello!") throw new Exception();
            if (list[0].my_second.lol.my_datetime == null) throw new Exception();
            if (list[0].my_second.lol.random_data != "AHOJ!hello!") throw new Exception();

            connection.Close();
        }

    }
    public class OtherClass
    {
        public DateTime my_datetime;
        public String random_data;
    }
    public class MyEntity
    {
        public int my_num;
        public string my_string;
        public bool my_bool;
        public OtherClass lol;

        public MyEntity my_second;
    }
}
