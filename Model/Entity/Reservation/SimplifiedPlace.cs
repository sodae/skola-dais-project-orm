﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Entity.Service;

namespace Model.Entity.Reservation
{
    public class SimplifiedPlace
    {
        public int ServiceId;
        public int WagonOrder;
        public int SeatNumber;

        public bool isTheSeat(Seat seat)
        {
            return this.SeatNumber == seat.SeatNumber
                && this.WagonOrder == seat.Wagon.Order
                && this.ServiceId == seat.Wagon.Service.Id;
        }
    }
}
