﻿using System;
using System.Collections.Generic;

namespace Model.Entity.Reservation
{
    public class Reservation
    {
        public string Code;
        public string Name;
        public string Email;
        public bool Paid;
        public DateTime Created;
        public Collection<Place> Places;
        public Collection<SimplifiedPlace> SimplifiedPlaces;
    }
}
