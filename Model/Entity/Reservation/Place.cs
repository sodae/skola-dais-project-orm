﻿using Model.Entity.Service;

namespace Model.Entity.Reservation
{
    public class Place
    {
        public double Price;
        public double Status;
        public Reservation Reservation;
        public Seat Seat;
    }
}
