﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Entity.Reservation
{
    public class SearchServiceResult
    {
        public DateTime Arrival;
        public DateTime Departure;
        public Service.Service Service;
    }
}
