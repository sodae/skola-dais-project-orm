﻿namespace Model.Entity
{
    public class Train
    {
        public readonly string Code;
        public readonly string Title;
        public readonly EngineDriver EngineDriver;

        public Train()
        {
        }

        public Train(string code, string title, EngineDriver engineDriver)
        {
            Code = code;
            Title = title;
            EngineDriver = engineDriver;
        }
    }
}