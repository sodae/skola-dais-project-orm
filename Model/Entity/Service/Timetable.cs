﻿using System;

namespace Model.Entity.Service
{
    public class Timetable
    {
        public DateTime Arrival { get; private set; }
        public Nullable<DateTime> Departure { get; private set; }

        public Station Station;
    }
}
