﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Model.Entity.Service
{
    public class Service
    {
        public int Id { get; private set; }
        public string Type { get; set; }
        public string Code { get; set; }
        public Train Train { get; set; }
        public readonly Collection<Timetable> Timetable;

        public Service(int id) : this()
        {
            Id = id;
        }

        public Service()
        {
            this.Timetable = new Collection<Timetable>();
        }
    }
}
