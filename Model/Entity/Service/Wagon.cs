﻿using System.Collections.Generic;

namespace Model.Entity.Service
{
    public class Wagon
    {
        public readonly int Order;
        public string Name;
        public string Type;
        public int Size; // table: seats
        public List<Seat> Seats;
        public Service Service;

        public Wagon()
        {
        }

        public Wagon(int order)
        {
            Order = order;
        }
    }
}
