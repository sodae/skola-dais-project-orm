﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Entity.Reservation;

namespace Model.Entity.Service
{
    public class Seat
    {
        public int SeatNumber;
        public string Type;
        public Wagon Wagon;
        public Place Reservation;
    }
}
