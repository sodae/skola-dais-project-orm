﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Entity
{
    public class EngineDriver
    {
        public readonly int Id;
        public readonly string Name;
        public readonly Train Train;

        public EngineDriver()
        {
        }

        public EngineDriver(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
