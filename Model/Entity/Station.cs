﻿namespace Model.Entity
{
    public class Station
    {
        public readonly string Code;
        public readonly string Name;

        public Station()
        {
        }

        public Station(string code, string name)
        {
            Code = code;
            Name = name;
        }
    }
}
