﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
using Model.Repository;

namespace gui
{
    public partial class ViewReservation : Form
    {
        private Connection connection;
        private ServiceRepository serviceRepository;
        private ReservationRepository reservationRepository;

        public ViewReservation()
        {
            InitializeComponent();
            connection = new Connection();
            connection.Open();
            this.serviceRepository = new ServiceRepository(connection);
            this.reservationRepository = new ReservationRepository(connection);

            this.servicesInGridList = new List<int>();
        }

        private Model.Entity.Reservation.Reservation selectedReservation;
        private int selectedServiceId = -1;

        private void ViewReservation_Load(object sender, EventArgs e)
        {
            this.service_seats.ItemCheck += (s, args) =>
            {
                if (args.CurrentValue == CheckState.Indeterminate) args.NewValue = CheckState.Indeterminate;
            };
            reloadReservationList();
            this.search_results.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.search_results_CellClick);
        }

        private void loadSeats()
        {
            this.service_seats.Items.Clear();
            foreach (var p in this.selectedReservation.SimplifiedPlaces)
            {
                if (p.ServiceId != this.selectedServiceId) continue;
                int i = this.service_seats.Items.Add("Vagón: " + p.WagonOrder.ToString() + ", místo: " + p.SeatNumber.ToString());
                this.service_seats.SetItemCheckState(i, CheckState.Checked);
                this.service_seats.SetItemCheckState(i, CheckState.Indeterminate);
            }
        }

        private List<int> servicesInGridList; 

        private void search_submit_Click(object sender, EventArgs e)
        {
            Model.Entity.Reservation.Reservation reservation = this.reservationRepository.GetReservation(this.search_code.Text);
            if (reservation == null)
            {
                MessageBox.Show("Rezervace nebyla nalazena");
                return;
            }

            this.selectedReservation = reservation;
            this.reloadReservation();
        }

        private void search_results_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            selectedServiceId = this.servicesInGridList[e.RowIndex];
            loadSeats();
        }


        private void reservation_submit_Click(object sender, EventArgs e)
        {
            if (selectedReservation == null)
            {
                MessageBox.Show("Není co ukládat, vyberte rezervaci");
                return;
            }

            if (string.IsNullOrWhiteSpace(this.reservation_name.Text) ||
                string.IsNullOrWhiteSpace(this.reservation_email.Text))
            {
                MessageBox.Show("Vyplnte jméno a email!");
                return;
            }

            selectedReservation.Name = this.reservation_name.Text;
            selectedReservation.Email = this.reservation_email.Text = this.reservation_email.Text;
            selectedReservation.Paid = this.reservation_paid.Checked;

            try
            {
                this.reservationRepository.updateReservation(selectedReservation);
                MessageBox.Show("Upraveno");
            }
            catch (Exception)
            {
                MessageBox.Show("Něco se podělalo.");
            }

        }

        private void new_reservation_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var w = new Reservation();
            w.FormClosed += delegate {
                this.reloadReservationList();
            };
            w.Show();
        }

        private void reservation_cancel_Click(object sender, EventArgs e)
        {
            if (this.selectedReservation == null)
            {
                MessageBox.Show("Není vybrána rezervace");
                return;
            }

            try
            {
                this.reservationRepository.cancelReservation(this.selectedReservation.Code);
                MessageBox.Show(String.Format("Rezervace {0} zrušena", this.selectedReservation.Code));
                this.selectedReservation = null;
            }
            catch (Exception)
            {
                MessageBox.Show("Něco se podělalo...");
            }
            this.reloadReservation();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.selectedReservation == null || this.selectedServiceId == -1)
            {
                MessageBox.Show("Není vybrána rezervace a nebo spoj");
                return;
            }

            try
            {
                this.reservationRepository.cancelReservation(this.selectedReservation.Code, selectedServiceId);
                MessageBox.Show(String.Format("Rezervace  {0} pro spoj {1} zrušena", this.selectedReservation.Code, this.selectedServiceId));
                if (this.reservationRepository.GetReservation(this.selectedReservation.Code) == null)
                {
                    this.selectedReservation = null;
                }
                this.selectedServiceId = -1;
            }
            catch (Exception)
            {
                MessageBox.Show("Něco se podělalo...");
            }
            this.reloadReservation();
        }


        private void reloadReservation()
        {
            this.service_seats.Items.Clear();
            this.search_results.Rows.Clear();
            this.servicesInGridList.Clear();
            this.reservation_name.Text = "";
            this.reservation_email.Text = "";
            this.reservation_paid.Checked = false;

            if (this.selectedReservation == null)
            {
                return;
            }

            Dictionary<int, int> result = new Dictionary<int, int>();

            foreach (var place in this.selectedReservation.SimplifiedPlaces)
            {
                result[place.ServiceId] = result.ContainsKey(place.ServiceId) ? result[place.ServiceId] + 1 : 1;
            }

            foreach (var r in result)
            {
                this.search_results.Rows.Add(new[] {
                    r.Key.ToString(),
                    r.Value.ToString()
                });
                this.servicesInGridList.Add(r.Key);
            }

            this.reservation_name.Text = selectedReservation.Name;
            this.reservation_email.Text = selectedReservation.Email;
            this.reservation_paid.Checked = selectedReservation.Paid;
        }

        private void reloadReservationList()
        {
            AutoCompleteStringCollection col = new AutoCompleteStringCollection();
            foreach (var r in this.reservationRepository.FindReservation())
            {
                col.Add(r.Code);
            }
            this.search_code.AutoCompleteCustomSource = col;
        }
    }
}
