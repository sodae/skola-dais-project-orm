﻿namespace gui
{
    partial class Reservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.search_from = new System.Windows.Forms.TextBox();
            this.search_to = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.search_submit = new System.Windows.Forms.Button();
            this.search_results = new System.Windows.Forms.DataGridView();
            this.service_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.arrival = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.search_date = new System.Windows.Forms.DateTimePicker();
            this.wagon_seats = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.reservation_view = new System.Windows.Forms.RichTextBox();
            this.Rezervování = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.reservation_name = new System.Windows.Forms.TextBox();
            this.reservation_email = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.reservation_submit = new System.Windows.Forms.Button();
            this.service_cancel = new System.Windows.Forms.Button();
            this.clone_submit = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.clone_datetime = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.search_results)).BeginInit();
            this.SuspendLayout();
            // 
            // search_from
            // 
            this.search_from.Location = new System.Drawing.Point(12, 68);
            this.search_from.Name = "search_from";
            this.search_from.Size = new System.Drawing.Size(257, 20);
            this.search_from.TabIndex = 0;
            // 
            // search_to
            // 
            this.search_to.Location = new System.Drawing.Point(12, 111);
            this.search_to.Name = "search_to";
            this.search_to.Size = new System.Drawing.Size(257, 20);
            this.search_to.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ze stanice";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Do stanice";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label3.Location = new System.Drawing.Point(6, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 31);
            this.label3.TabIndex = 4;
            this.label3.Text = "Vyhledávání";
            // 
            // search_submit
            // 
            this.search_submit.Location = new System.Drawing.Point(12, 165);
            this.search_submit.Name = "search_submit";
            this.search_submit.Size = new System.Drawing.Size(257, 23);
            this.search_submit.TabIndex = 5;
            this.search_submit.Text = "Vyhledat";
            this.search_submit.UseVisualStyleBackColor = true;
            this.search_submit.Click += new System.EventHandler(this.search_submit_Click);
            // 
            // search_results
            // 
            this.search_results.AllowUserToAddRows = false;
            this.search_results.AllowUserToDeleteRows = false;
            this.search_results.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.search_results.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.service_id,
            this.departure,
            this.arrival});
            this.search_results.Location = new System.Drawing.Point(12, 194);
            this.search_results.Name = "search_results";
            this.search_results.ReadOnly = true;
            this.search_results.RowHeadersVisible = false;
            this.search_results.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.search_results.Size = new System.Drawing.Size(257, 199);
            this.search_results.TabIndex = 6;
            // 
            // service_id
            // 
            this.service_id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.service_id.Frozen = true;
            this.service_id.HeaderText = "Spoj";
            this.service_id.Name = "service_id";
            this.service_id.ReadOnly = true;
            this.service_id.Width = 53;
            // 
            // departure
            // 
            this.departure.Frozen = true;
            this.departure.HeaderText = "Odjezd";
            this.departure.Name = "departure";
            this.departure.ReadOnly = true;
            // 
            // arrival
            // 
            this.arrival.Frozen = true;
            this.arrival.HeaderText = "Příjezd";
            this.arrival.Name = "arrival";
            this.arrival.ReadOnly = true;
            // 
            // search_date
            // 
            this.search_date.Location = new System.Drawing.Point(12, 139);
            this.search_date.Name = "search_date";
            this.search_date.Size = new System.Drawing.Size(257, 20);
            this.search_date.TabIndex = 7;
            // 
            // wagon_seats
            // 
            this.wagon_seats.FormattingEnabled = true;
            this.wagon_seats.Location = new System.Drawing.Point(283, 59);
            this.wagon_seats.Name = "wagon_seats";
            this.wagon_seats.Size = new System.Drawing.Size(200, 259);
            this.wagon_seats.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label4.Location = new System.Drawing.Point(277, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 31);
            this.label4.TabIndex = 10;
            this.label4.Text = "Místa";
            // 
            // reservation_view
            // 
            this.reservation_view.Location = new System.Drawing.Point(504, 59);
            this.reservation_view.Name = "reservation_view";
            this.reservation_view.ReadOnly = true;
            this.reservation_view.Size = new System.Drawing.Size(184, 226);
            this.reservation_view.TabIndex = 11;
            this.reservation_view.Text = "";
            // 
            // Rezervování
            // 
            this.Rezervování.AutoSize = true;
            this.Rezervování.Location = new System.Drawing.Point(501, 288);
            this.Rezervování.Name = "Rezervování";
            this.Rezervování.Size = new System.Drawing.Size(62, 13);
            this.Rezervování.TabIndex = 12;
            this.Rezervování.Text = "Vaše jméno";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label5.Location = new System.Drawing.Point(498, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(187, 31);
            this.label5.TabIndex = 13;
            this.label5.Text = "Vybraná místa";
            // 
            // reservation_name
            // 
            this.reservation_name.Location = new System.Drawing.Point(504, 304);
            this.reservation_name.Name = "reservation_name";
            this.reservation_name.Size = new System.Drawing.Size(184, 20);
            this.reservation_name.TabIndex = 14;
            // 
            // reservation_email
            // 
            this.reservation_email.Location = new System.Drawing.Point(504, 343);
            this.reservation_email.Name = "reservation_email";
            this.reservation_email.Size = new System.Drawing.Size(184, 20);
            this.reservation_email.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(501, 327);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Vaše email";
            // 
            // reservation_submit
            // 
            this.reservation_submit.Location = new System.Drawing.Point(504, 369);
            this.reservation_submit.Name = "reservation_submit";
            this.reservation_submit.Size = new System.Drawing.Size(184, 23);
            this.reservation_submit.TabIndex = 17;
            this.reservation_submit.Text = "Rezervovat";
            this.reservation_submit.UseVisualStyleBackColor = true;
            this.reservation_submit.Click += new System.EventHandler(this.reservation_submit_Click);
            // 
            // service_cancel
            // 
            this.service_cancel.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.service_cancel.Location = new System.Drawing.Point(372, 22);
            this.service_cancel.Name = "service_cancel";
            this.service_cancel.Size = new System.Drawing.Size(111, 22);
            this.service_cancel.TabIndex = 29;
            this.service_cancel.Text = "Přidat vagon";
            this.service_cancel.UseVisualStyleBackColor = true;
            this.service_cancel.Click += new System.EventHandler(this.service_cancel_Click);
            // 
            // clone_submit
            // 
            this.clone_submit.Location = new System.Drawing.Point(283, 370);
            this.clone_submit.Name = "clone_submit";
            this.clone_submit.Size = new System.Drawing.Size(200, 23);
            this.clone_submit.TabIndex = 30;
            this.clone_submit.Text = "Klonovat spoj";
            this.clone_submit.UseVisualStyleBackColor = true;
            this.clone_submit.Click += new System.EventHandler(this.clone_submit_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(280, 327);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(204, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Klonování spoje, přijezd do první  stanice";
            // 
            // clone_datetime
            // 
            this.clone_datetime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.clone_datetime.Location = new System.Drawing.Point(283, 344);
            this.clone_datetime.Name = "clone_datetime";
            this.clone_datetime.Size = new System.Drawing.Size(200, 20);
            this.clone_datetime.TabIndex = 32;
            // 
            // Reservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 405);
            this.Controls.Add(this.clone_datetime);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.clone_submit);
            this.Controls.Add(this.service_cancel);
            this.Controls.Add(this.reservation_submit);
            this.Controls.Add(this.reservation_email);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.reservation_name);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Rezervování);
            this.Controls.Add(this.reservation_view);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.wagon_seats);
            this.Controls.Add(this.search_date);
            this.Controls.Add(this.search_results);
            this.Controls.Add(this.search_submit);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.search_to);
            this.Controls.Add(this.search_from);
            this.Name = "Reservation";
            this.Text = "Rezervování";
            this.Load += new System.EventHandler(this.Reservation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.search_results)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox search_from;
        private System.Windows.Forms.TextBox search_to;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button search_submit;
        private System.Windows.Forms.DataGridView search_results;
        private System.Windows.Forms.DateTimePicker search_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn service_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn departure;
        private System.Windows.Forms.DataGridViewTextBoxColumn arrival;
        private System.Windows.Forms.CheckedListBox wagon_seats;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox reservation_view;
        private System.Windows.Forms.Label Rezervování;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox reservation_name;
        private System.Windows.Forms.TextBox reservation_email;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button reservation_submit;
        private System.Windows.Forms.Button service_cancel;
        private System.Windows.Forms.Button clone_submit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker clone_datetime;
    }
}

