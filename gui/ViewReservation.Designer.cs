﻿namespace gui
{
    partial class ViewReservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.service_seats = new System.Windows.Forms.CheckedListBox();
            this.search_results = new System.Windows.Forms.DataGridView();
            this.service_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.search_submit = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.search_code = new System.Windows.Forms.TextBox();
            this.reservation_submit = new System.Windows.Forms.Button();
            this.reservation_email = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.reservation_name = new System.Windows.Forms.TextBox();
            this.Rezervování = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.reservation_paid = new System.Windows.Forms.CheckBox();
            this.reservation_cancel = new System.Windows.Forms.Button();
            this.new_reservation = new System.Windows.Forms.LinkLabel();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.search_results)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label4.Location = new System.Drawing.Point(283, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 31);
            this.label4.TabIndex = 17;
            this.label4.Text = "Místa";
            // 
            // service_seats
            // 
            this.service_seats.FormattingEnabled = true;
            this.service_seats.Location = new System.Drawing.Point(289, 55);
            this.service_seats.Name = "service_seats";
            this.service_seats.Size = new System.Drawing.Size(200, 334);
            this.service_seats.TabIndex = 16;
            // 
            // search_results
            // 
            this.search_results.AllowUserToAddRows = false;
            this.search_results.AllowUserToDeleteRows = false;
            this.search_results.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.search_results.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.service_id,
            this.departure});
            this.search_results.Location = new System.Drawing.Point(18, 119);
            this.search_results.Name = "search_results";
            this.search_results.ReadOnly = true;
            this.search_results.RowHeadersVisible = false;
            this.search_results.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.search_results.Size = new System.Drawing.Size(257, 270);
            this.search_results.TabIndex = 15;
            // 
            // service_id
            // 
            this.service_id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.service_id.HeaderText = "Spoj";
            this.service_id.Name = "service_id";
            this.service_id.ReadOnly = true;
            // 
            // departure
            // 
            this.departure.HeaderText = "Počet míst";
            this.departure.Name = "departure";
            this.departure.ReadOnly = true;
            // 
            // search_submit
            // 
            this.search_submit.Location = new System.Drawing.Point(18, 90);
            this.search_submit.Name = "search_submit";
            this.search_submit.Size = new System.Drawing.Size(257, 23);
            this.search_submit.TabIndex = 14;
            this.search_submit.Text = "Zobrazit rezervaci";
            this.search_submit.UseVisualStyleBackColor = true;
            this.search_submit.Click += new System.EventHandler(this.search_submit_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 31);
            this.label3.TabIndex = 13;
            this.label3.Text = "Vyhledávání";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Kód rezervace";
            // 
            // search_code
            // 
            this.search_code.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.search_code.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.search_code.Location = new System.Drawing.Point(18, 64);
            this.search_code.Name = "search_code";
            this.search_code.Size = new System.Drawing.Size(257, 20);
            this.search_code.TabIndex = 11;
            // 
            // reservation_submit
            // 
            this.reservation_submit.Location = new System.Drawing.Point(503, 159);
            this.reservation_submit.Name = "reservation_submit";
            this.reservation_submit.Size = new System.Drawing.Size(184, 23);
            this.reservation_submit.TabIndex = 23;
            this.reservation_submit.Text = "Uložit";
            this.reservation_submit.UseVisualStyleBackColor = true;
            this.reservation_submit.Click += new System.EventHandler(this.reservation_submit_Click);
            // 
            // reservation_email
            // 
            this.reservation_email.Location = new System.Drawing.Point(503, 110);
            this.reservation_email.Name = "reservation_email";
            this.reservation_email.Size = new System.Drawing.Size(184, 20);
            this.reservation_email.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(500, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Email";
            // 
            // reservation_name
            // 
            this.reservation_name.Location = new System.Drawing.Point(503, 71);
            this.reservation_name.Name = "reservation_name";
            this.reservation_name.Size = new System.Drawing.Size(184, 20);
            this.reservation_name.TabIndex = 20;
            // 
            // Rezervování
            // 
            this.Rezervování.AutoSize = true;
            this.Rezervování.Location = new System.Drawing.Point(500, 55);
            this.Rezervování.Name = "Rezervování";
            this.Rezervování.Size = new System.Drawing.Size(38, 13);
            this.Rezervování.TabIndex = 19;
            this.Rezervování.Text = "Jméno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label2.Location = new System.Drawing.Point(497, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 31);
            this.label2.TabIndex = 24;
            this.label2.Text = "Rezervace";
            // 
            // reservation_paid
            // 
            this.reservation_paid.AutoSize = true;
            this.reservation_paid.Location = new System.Drawing.Point(503, 136);
            this.reservation_paid.Name = "reservation_paid";
            this.reservation_paid.Size = new System.Drawing.Size(77, 17);
            this.reservation_paid.TabIndex = 26;
            this.reservation_paid.Text = "Zaplaceno";
            this.reservation_paid.UseVisualStyleBackColor = true;
            // 
            // reservation_cancel
            // 
            this.reservation_cancel.ForeColor = System.Drawing.Color.Red;
            this.reservation_cancel.Location = new System.Drawing.Point(503, 188);
            this.reservation_cancel.Name = "reservation_cancel";
            this.reservation_cancel.Size = new System.Drawing.Size(184, 23);
            this.reservation_cancel.TabIndex = 27;
            this.reservation_cancel.Text = "Zrušit celou rezervaci";
            this.reservation_cancel.UseVisualStyleBackColor = true;
            this.reservation_cancel.Click += new System.EventHandler(this.reservation_cancel_Click);
            // 
            // new_reservation
            // 
            this.new_reservation.AutoSize = true;
            this.new_reservation.Location = new System.Drawing.Point(199, 24);
            this.new_reservation.Name = "new_reservation";
            this.new_reservation.Size = new System.Drawing.Size(76, 13);
            this.new_reservation.TabIndex = 28;
            this.new_reservation.TabStop = true;
            this.new_reservation.Text = "Vytvořit novou";
            this.new_reservation.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.new_reservation_LinkClicked);
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(503, 217);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(184, 23);
            this.button1.TabIndex = 29;
            this.button1.Text = "Zrušit rezervaci pro vybraný spoj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ViewReservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 403);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.new_reservation);
            this.Controls.Add(this.reservation_cancel);
            this.Controls.Add(this.reservation_paid);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.reservation_submit);
            this.Controls.Add(this.reservation_email);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.reservation_name);
            this.Controls.Add(this.Rezervování);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.service_seats);
            this.Controls.Add(this.search_results);
            this.Controls.Add(this.search_submit);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.search_code);
            this.Name = "ViewReservation";
            this.Text = "Rezervační systém";
            this.Load += new System.EventHandler(this.ViewReservation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.search_results)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckedListBox service_seats;
        private System.Windows.Forms.DataGridView search_results;
        private System.Windows.Forms.Button search_submit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox search_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn service_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn departure;
        private System.Windows.Forms.Button reservation_submit;
        private System.Windows.Forms.TextBox reservation_email;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox reservation_name;
        private System.Windows.Forms.Label Rezervování;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox reservation_paid;
        private System.Windows.Forms.Button reservation_cancel;
        private System.Windows.Forms.LinkLabel new_reservation;
        private System.Windows.Forms.Button button1;
    }
}