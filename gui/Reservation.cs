﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
using Model.Entity.Reservation;
using Model.Entity.Service;
using Model.Repository;

namespace gui
{
    public partial class Reservation : Form
    {
        private Connection connection;
        private ServiceRepository serviceRepository;
        private ReservationRepository reservationRepository;

        public Reservation()
        {
            InitializeComponent();
            connection = new Connection();
            connection.Open();
            this.serviceRepository = new ServiceRepository(connection);
            this.reservationRepository = new ReservationRepository(connection);

            this.seatsInList = new List<Seat>();
            this.reservations = new List<SimplifiedPlace>();
        }

        ~Reservation()
        {
            connection.Close();
        }

        private void setUpProtype()
        {
            this.search_from.Text = "ostrava";
            this.search_to.Text = "praha";
            this.reservation_name.Text = "Martin";
            this.reservation_email.Text = "s@example.cz";
            this.search_date.Value = new DateTime(2014, 10, 1);
        }

        private void Reservation_Load(object sender, EventArgs e)
        {
            this.wagon_seats.ItemCheck += (s, args) =>
            {
                if (args.CurrentValue == CheckState.Indeterminate) args.NewValue = CheckState.Indeterminate;
            };
            this.wagon_seats.ItemCheck += (s, args) =>
            {
                if (!this.listLoaded) return;

                if (args.CurrentValue == CheckState.Unchecked)
                {
                    this.reservations.Add(new SimplifiedPlace()
                    {
                        ServiceId = seatsInList[args.Index].Wagon.Service.Id,
                        WagonOrder = seatsInList[args.Index].Wagon.Order,
                        SeatNumber = seatsInList[args.Index].SeatNumber,
                    });
                }
                else if (args.CurrentValue == CheckState.Checked)
                {
                    List<SimplifiedPlace> toRemove = new List<SimplifiedPlace>();
                    foreach (var r in reservations)
                    {
                        if (r.isTheSeat(this.seatsInList[args.Index]))
                        {
                            toRemove.Add(r);
                        }
                    }
                    foreach (var r in toRemove)
                    {
                        reservations.Remove(r);
                    }
                }
                this.refreshView();
            };
            this.search_results.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.clone_datetime.CustomFormat = "dd. MM. yyyy hh:mm:ss";  
            this.setUpProtype(); // comment me!
        }

        private void refreshView()
        {
            this.reservation_view.Clear();
            foreach (var r in this.reservations)
            {
                this.reservation_view.Text += String.Format("Spoj {0}, Vagon {1}, Místo {2}\n", r.ServiceId, r.WagonOrder, r.SeatNumber);
            }
        }

        private List<SimplifiedPlace> reservations;
        private List<Seat> seatsInList;
        private bool listLoaded = false;

        private void loadSeats()
        {
            this.listLoaded = false;
            this.wagon_seats.Items.Clear();
            this.seatsInList.Clear();

            var seats = this.serviceRepository.FindSeatByService(this.selectedService.Service.Id);
            foreach (var seat in seats)
            {
                this.seatsInList.Add(seat);
                int i = this.wagon_seats.Items.Add("Vagon: " + seat.Wagon.Order.ToString() + ", místo: " + seat.SeatNumber.ToString());
                if (seat.Reservation.Reservation.Code != null)
                {
                    this.wagon_seats.SetItemCheckState(i, CheckState.Indeterminate);
                }
                else
                {
                    foreach (var r in reservations)
                    {
                        if (r.isTheSeat(seat))
                        {
                            this.wagon_seats.SetItemCheckState(i, CheckState.Checked);
                        }
                    }
                }
            }
            this.listLoaded = true;
        }

        private SearchServiceResult selectedService;
        private List<SearchServiceResult> resultsServices; 

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedService = resultsServices[e.RowIndex];
            this.loadSeats();
        }



        private void search_submit_Click(object sender, EventArgs e)
        {
            if (this.search_from.Text == "" || this.search_to.Text == "")
            {
                MessageBox.Show("Nezadali jste stanici");
                return;
            }

            this.resultsServices = new List<SearchServiceResult>();
            this.search_results.Rows.Clear();

            var data = this.serviceRepository.FindServiceByStation(this.search_from.Text, this.search_to.Text, false, this.search_date.Value);
            foreach (SearchServiceResult result in data)
            {
                this.search_results.Rows.Add(new[]
                {
                    result.Service.Id.ToString(), 
                    result.Arrival.ToString("g"), 
                    result.Departure.ToString("g")
                });
                this.resultsServices.Add(result);
            }
        }

        private void reservation_submit_Click(object sender, EventArgs e)
        {
            if (this.reservation_name.Text == "" || this.reservation_email.Text == "")
            {
                MessageBox.Show("Nezadali jste jméno nebo email");
                return;
            }
            if (!this.reservations.Any())
            {
                MessageBox.Show("Nemáte vybrané žádnou rezervaci");
                return;
            }

            try
            {
                string code = this.reservationRepository.CreateReservation(
                    this.reservation_name.Text,
                    this.reservation_email.Text,
                    this.reservations);
                MessageBox.Show("Kod vaší rezervace: " + code);
                Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Při rezervaci nastala chyba!");
            }
        }

        private void service_cancel_Click(object sender, EventArgs e)
        {
            if (this.selectedService == null)
            {
                MessageBox.Show("Není co rozširovat, vyberte spoj");
                return;
            }
            try
            {
                this.serviceRepository.ExtendService(this.selectedService.Service.Id, 10);
            }
            catch (Exception)
            {
                MessageBox.Show("Při rezervaci nastala chyba!");
            }
            this.loadSeats();
        }

        private void clone_submit_Click(object sender, EventArgs e)
        {
            if (this.selectedService == null)
            {
                MessageBox.Show("Není co rozširovat, vyberte spoj");
                return;
            }
            try
            {
                this.serviceRepository.CloneService(this.selectedService.Service.Id, this.clone_datetime.Value);
                MessageBox.Show("Klonování spoje, bylo uspěšné");
            }
            catch (Exception)
            {
                MessageBox.Show("Při rezervaci nastala chyba!");
            }
            
        }


    }
}
