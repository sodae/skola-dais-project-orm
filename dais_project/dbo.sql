/*
Navicat SQL Server Data Transfer

Source Server         : MS-server-localhost
Source Server Version : 100000
Source Host           : SODAE-V3\SQLEXPRESS:1433
Source Database       : dais_2
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 100000
File Encoding         : 65001

Date: 2015-04-28 17:04:22
*/


-- ----------------------------
-- Table structure for engine_driver
-- ----------------------------

GO
CREATE TABLE [dbo].[engine_driver] (
[id] int NOT NULL IDENTITY(1,1) NOT FOR REPLICATION ,
[name] nvarchar(40) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[engine_driver]', RESEED, 2)
GO

-- ----------------------------
-- Records of engine_driver
-- ----------------------------
SET IDENTITY_INSERT [dbo].[engine_driver] ON
GO
INSERT INTO [dbo].[engine_driver] ([id], [name]) VALUES (N'1', N'Lucie Kuchárová')
GO
GO
INSERT INTO [dbo].[engine_driver] ([id], [name]) VALUES (N'2', N'Adam Stříž')
GO
GO
SET IDENTITY_INSERT [dbo].[engine_driver] OFF
GO

-- ----------------------------
-- Table structure for reservation
-- ----------------------------

GO
CREATE TABLE [dbo].[reservation] (
[code] nvarchar(40) NOT NULL ,
[name] nvarchar(100) NULL ,
[email] nvarchar(200) NOT NULL ,
[paid] tinyint NOT NULL ,
[created] datetime2(7) NOT NULL 
)


GO

-- ----------------------------
-- Records of reservation
-- ----------------------------
INSERT INTO [dbo].[reservation] ([code], [name], [email], [paid], [created]) VALUES (N'1f3s', N'Petr Lukášš', N'petr@lukas.cz', N'1', N'2014-12-01 14:49:16.0000000')
GO
GO
INSERT INTO [dbo].[reservation] ([code], [name], [email], [paid], [created]) VALUES (N'72766EE72', N'Martin', N's@ex.cz', N'0', N'2015-04-28 16:36:50.7370000')
GO
GO
INSERT INTO [dbo].[reservation] ([code], [name], [email], [paid], [created]) VALUES (N'8AB53755E', N'Martin', N's@ex.cz', N'0', N'2015-04-28 16:10:21.1030000')
GO
GO
INSERT INTO [dbo].[reservation] ([code], [name], [email], [paid], [created]) VALUES (N'B0A8D1403', N'Martin', N'jiny@email.cz', N'1', N'2015-04-28 16:42:03.8300000')
GO
GO
INSERT INTO [dbo].[reservation] ([code], [name], [email], [paid], [created]) VALUES (N'ee9a1', N'Martin', N's@ex.cz', N'0', N'2015-04-28 15:30:48.3868890')
GO
GO
INSERT INTO [dbo].[reservation] ([code], [name], [email], [paid], [created]) VALUES (N'f7215', N'Martin', N's@ex.cz', N'0', N'2015-04-28 15:38:23.1185453')
GO
GO

-- ----------------------------
-- Table structure for reservation_place
-- ----------------------------

GO
CREATE TABLE [dbo].[reservation_place] (
[reservation_code] nvarchar(40) NOT NULL ,
[wagon_order] int NOT NULL ,
[seat_number] int NOT NULL ,
[service_id] int NOT NULL ,
[status] int NOT NULL ,
[price] int NOT NULL 
)


GO

-- ----------------------------
-- Records of reservation_place
-- ----------------------------
INSERT INTO [dbo].[reservation_place] ([reservation_code], [wagon_order], [seat_number], [service_id], [status], [price]) VALUES (N'1f3s', N'1', N'2', N'3', N'1', N'300')
GO
GO
INSERT INTO [dbo].[reservation_place] ([reservation_code], [wagon_order], [seat_number], [service_id], [status], [price]) VALUES (N'ee9a1', N'2', N'3', N'3', N'0', N'100')
GO
GO
INSERT INTO [dbo].[reservation_place] ([reservation_code], [wagon_order], [seat_number], [service_id], [status], [price]) VALUES (N'72766EE72', N'3', N'4', N'3', N'0', N'100')
GO
GO
INSERT INTO [dbo].[reservation_place] ([reservation_code], [wagon_order], [seat_number], [service_id], [status], [price]) VALUES (N'1f3s', N'1', N'5', N'5', N'1', N'500')
GO
GO
INSERT INTO [dbo].[reservation_place] ([reservation_code], [wagon_order], [seat_number], [service_id], [status], [price]) VALUES (N'72766EE72', N'2', N'5', N'3', N'0', N'100')
GO
GO
INSERT INTO [dbo].[reservation_place] ([reservation_code], [wagon_order], [seat_number], [service_id], [status], [price]) VALUES (N'B0A8D1403', N'3', N'5', N'3', N'0', N'100')
GO
GO
INSERT INTO [dbo].[reservation_place] ([reservation_code], [wagon_order], [seat_number], [service_id], [status], [price]) VALUES (N'B0A8D1403', N'3', N'6', N'3', N'0', N'100')
GO
GO
INSERT INTO [dbo].[reservation_place] ([reservation_code], [wagon_order], [seat_number], [service_id], [status], [price]) VALUES (N'8AB53755E', N'3', N'7', N'3', N'0', N'100')
GO
GO
INSERT INTO [dbo].[reservation_place] ([reservation_code], [wagon_order], [seat_number], [service_id], [status], [price]) VALUES (N'f7215', N'3', N'8', N'3', N'0', N'100')
GO
GO
INSERT INTO [dbo].[reservation_place] ([reservation_code], [wagon_order], [seat_number], [service_id], [status], [price]) VALUES (N'f7215', N'3', N'9', N'3', N'0', N'100')
GO
GO
INSERT INTO [dbo].[reservation_place] ([reservation_code], [wagon_order], [seat_number], [service_id], [status], [price]) VALUES (N'f7215', N'3', N'10', N'3', N'0', N'100')
GO
GO

-- ----------------------------
-- Table structure for station
-- ----------------------------

GO
CREATE TABLE [dbo].[station] (
[name] nvarchar(50) NOT NULL ,
[code] nvarchar(10) NOT NULL 
)


GO

-- ----------------------------
-- Records of station
-- ----------------------------
INSERT INTO [dbo].[station] ([name], [code]) VALUES (N'Havířov', N'havirov')
GO
GO
INSERT INTO [dbo].[station] ([name], [code]) VALUES (N'Olomouc', N'olomouc')
GO
GO
INSERT INTO [dbo].[station] ([name], [code]) VALUES (N'Ostrava', N'ostrava')
GO
GO
INSERT INTO [dbo].[station] ([name], [code]) VALUES (N'Praha', N'praha')
GO
GO

-- ----------------------------
-- Table structure for timetable
-- ----------------------------

GO
CREATE TABLE [dbo].[timetable] (
[arrival] datetime2(7) NOT NULL ,
[departure] datetime2(7) NULL ,
[station_code] nvarchar(10) NOT NULL ,
[service_id] int NOT NULL 
)


GO

-- ----------------------------
-- Records of timetable
-- ----------------------------
INSERT INTO [dbo].[timetable] ([arrival], [departure], [station_code], [service_id]) VALUES (N'2014-12-07 14:08:00.0000000', N'2014-12-07 14:10:00.0000000', N'havirov', N'2')
GO
GO
INSERT INTO [dbo].[timetable] ([arrival], [departure], [station_code], [service_id]) VALUES (N'2014-12-07 22:00:00.0000000', null, N'havirov', N'4')
GO
GO
INSERT INTO [dbo].[timetable] ([arrival], [departure], [station_code], [service_id]) VALUES (N'2014-12-07 15:10:00.0000000', N'2014-12-07 15:15:00.0000000', N'olomouc', N'3')
GO
GO
INSERT INTO [dbo].[timetable] ([arrival], [departure], [station_code], [service_id]) VALUES (N'2015-04-28 17:22:39.0000000', N'2015-04-28 17:27:39.0000000', N'olomouc', N'9')
GO
GO
INSERT INTO [dbo].[timetable] ([arrival], [departure], [station_code], [service_id]) VALUES (N'2014-12-07 14:40:00.0000000', null, N'ostrava', N'2')
GO
GO
INSERT INTO [dbo].[timetable] ([arrival], [departure], [station_code], [service_id]) VALUES (N'2014-12-07 14:50:00.0000000', N'2014-12-07 14:55:00.0000000', N'ostrava', N'3')
GO
GO
INSERT INTO [dbo].[timetable] ([arrival], [departure], [station_code], [service_id]) VALUES (N'2014-12-07 21:30:00.0000000', N'2014-12-07 21:32:00.0000000', N'ostrava', N'4')
GO
GO
INSERT INTO [dbo].[timetable] ([arrival], [departure], [station_code], [service_id]) VALUES (N'2014-12-07 21:12:00.0000000', null, N'ostrava', N'5')
GO
GO
INSERT INTO [dbo].[timetable] ([arrival], [departure], [station_code], [service_id]) VALUES (N'2015-04-28 17:02:39.0000000', N'2015-04-28 17:07:39.0000000', N'ostrava', N'9')
GO
GO
INSERT INTO [dbo].[timetable] ([arrival], [departure], [station_code], [service_id]) VALUES (N'2014-12-07 17:10:00.0000000', null, N'praha', N'3')
GO
GO
INSERT INTO [dbo].[timetable] ([arrival], [departure], [station_code], [service_id]) VALUES (N'2014-12-07 17:20:00.0000000', N'2014-12-07 17:15:00.0000000', N'praha', N'5')
GO
GO
INSERT INTO [dbo].[timetable] ([arrival], [departure], [station_code], [service_id]) VALUES (N'2015-04-28 19:22:39.0000000', null, N'praha', N'9')
GO
GO

-- ----------------------------
-- Table structure for train
-- ----------------------------

GO
CREATE TABLE [dbo].[train] (
[title] nvarchar(255) NOT NULL ,
[code] nvarchar(10) NOT NULL ,
[engine_driver_id] int NOT NULL 
)


GO

-- ----------------------------
-- Records of train
-- ----------------------------
INSERT INTO [dbo].[train] ([title], [code], [engine_driver_id]) VALUES (N'Alfík', N'ALFA', N'1')
GO
GO
INSERT INTO [dbo].[train] ([title], [code], [engine_driver_id]) VALUES (N'Drums', N'DRUM', N'2')
GO
GO

-- ----------------------------
-- Table structure for train_service
-- ----------------------------

GO
CREATE TABLE [dbo].[train_service] (
[id] int NOT NULL IDENTITY(1,1) NOT FOR REPLICATION ,
[code] nvarchar(10) NOT NULL ,
[type] nvarchar(2) NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[train_service]', RESEED, 9)
GO

-- ----------------------------
-- Records of train_service
-- ----------------------------
SET IDENTITY_INSERT [dbo].[train_service] ON
GO
INSERT INTO [dbo].[train_service] ([id], [code], [type]) VALUES (N'2', N'ALFA', N'SP')
GO
GO
INSERT INTO [dbo].[train_service] ([id], [code], [type]) VALUES (N'3', N'DRUM', N'R')
GO
GO
INSERT INTO [dbo].[train_service] ([id], [code], [type]) VALUES (N'4', N'ALFA', N'OS')
GO
GO
INSERT INTO [dbo].[train_service] ([id], [code], [type]) VALUES (N'5', N'DRUM', N'EX')
GO
GO
INSERT INTO [dbo].[train_service] ([id], [code], [type]) VALUES (N'9', N'DRUM', N'R')
GO
GO
SET IDENTITY_INSERT [dbo].[train_service] OFF
GO

-- ----------------------------
-- Table structure for wagon
-- ----------------------------

GO
CREATE TABLE [dbo].[wagon] (
[name] nvarchar(50) NOT NULL ,
[seats] int NOT NULL ,
[type] nvarchar(10) NOT NULL ,
[order] int NOT NULL ,
[service_id] int NOT NULL 
)


GO

-- ----------------------------
-- Records of wagon
-- ----------------------------
INSERT INTO [dbo].[wagon] ([name], [seats], [type], [order], [service_id]) VALUES (N'Přední', N'10', N'2. třída', N'1', N'2')
GO
GO
INSERT INTO [dbo].[wagon] ([name], [seats], [type], [order], [service_id]) VALUES (N'Bussinness 1', N'2', N'Bussinness', N'1', N'3')
GO
GO
INSERT INTO [dbo].[wagon] ([name], [seats], [type], [order], [service_id]) VALUES (N'Přední', N'20', N'2. třída', N'1', N'4')
GO
GO
INSERT INTO [dbo].[wagon] ([name], [seats], [type], [order], [service_id]) VALUES (N'Business', N'5', N'Business', N'1', N'5')
GO
GO
INSERT INTO [dbo].[wagon] ([name], [seats], [type], [order], [service_id]) VALUES (N'Bussinness 1', N'2', N'Bussinness', N'1', N'9')
GO
GO
INSERT INTO [dbo].[wagon] ([name], [seats], [type], [order], [service_id]) VALUES (N'Zadní', N'10', N'1. třída', N'2', N'2')
GO
GO
INSERT INTO [dbo].[wagon] ([name], [seats], [type], [order], [service_id]) VALUES (N'Economy', N'5', N'Economy', N'2', N'3')
GO
GO
INSERT INTO [dbo].[wagon] ([name], [seats], [type], [order], [service_id]) VALUES (N'Economy', N'10', N'Economy', N'2', N'5')
GO
GO
INSERT INTO [dbo].[wagon] ([name], [seats], [type], [order], [service_id]) VALUES (N'Economy', N'5', N'Economy', N'2', N'9')
GO
GO
INSERT INTO [dbo].[wagon] ([name], [seats], [type], [order], [service_id]) VALUES (N'', N'10', N'Economy', N'3', N'3')
GO
GO
INSERT INTO [dbo].[wagon] ([name], [seats], [type], [order], [service_id]) VALUES (N'', N'10', N'Economy', N'3', N'9')
GO
GO

-- ----------------------------
-- Table structure for wagon_seat
-- ----------------------------

GO
CREATE TABLE [dbo].[wagon_seat] (
[wagon_order] int NOT NULL ,
[wagon_service_id] int NOT NULL ,
[seat_number] int NOT NULL ,
[type] nvarchar(10) NULL 
)


GO

-- ----------------------------
-- Records of wagon_seat
-- ----------------------------
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'2', N'1', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'3', N'1', N'B')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'1', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'5', N'1', N'B')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'9', N'1', N'B')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'2', N'1', N'N')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'3', N'1', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'5', N'1', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'9', N'1', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'3', N'1', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'9', N'1', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'2', N'2', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'3', N'2', N'B')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'2', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'5', N'2', N'B')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'9', N'2', N'B')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'2', N'2', N'N')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'3', N'2', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'5', N'2', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'9', N'2', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'3', N'2', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'9', N'2', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'2', N'3', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'3', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'5', N'3', N'B')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'2', N'3', N'N')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'3', N'3', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'5', N'3', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'9', N'3', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'3', N'3', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'9', N'3', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'2', N'4', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'4', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'5', N'4', N'B')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'2', N'4', N'N')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'3', N'4', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'5', N'4', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'9', N'4', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'3', N'4', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'9', N'4', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'2', N'5', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'5', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'5', N'5', N'B')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'2', N'5', N'N')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'3', N'5', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'5', N'5', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'9', N'5', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'3', N'5', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'9', N'5', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'2', N'6', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'6', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'2', N'6', N'N')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'5', N'6', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'3', N'6', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'9', N'6', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'2', N'7', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'7', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'2', N'7', N'N')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'5', N'7', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'3', N'7', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'9', N'7', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'2', N'8', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'8', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'2', N'8', N'N')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'5', N'8', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'3', N'8', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'9', N'8', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'2', N'9', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'9', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'2', N'9', N'N')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'5', N'9', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'3', N'9', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'9', N'9', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'2', N'10', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'10', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'2', N'10', N'N')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'2', N'5', N'10', N'E')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'3', N'10', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'3', N'9', N'10', null)
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'11', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'12', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'13', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'14', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'15', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'16', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'17', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'18', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'19', N'S')
GO
GO
INSERT INTO [dbo].[wagon_seat] ([wagon_order], [wagon_service_id], [seat_number], [type]) VALUES (N'1', N'4', N'20', N'S')
GO
GO

-- ----------------------------
-- Procedure structure for cancel_reservation
-- ----------------------------
DROP PROCEDURE [dbo].[cancel_reservation]
GO
CREATE PROCEDURE [dbo].[cancel_reservation]
	@code_reservation nvarchar(40),
	@service_id int = 0
AS
BEGIN
	
	DECLARE @reservation_count INT = (SELECT COUNT(*) FROM reservation WHERE code = @code_reservation);
	if (@reservation_count = 0) return 0; 

	if (@service_id > 0) 
		DELETE reservation_place WHERE reservation_code = @code_reservation AND service_id = @service_id;
	else
		DELETE reservation_place WHERE reservation_code = @code_reservation;

	SET @reservation_count = (SELECT COUNT(*) FROM reservation_place WHERE reservation_code = @code_reservation);
	if (@reservation_count = 0)
		DELETE reservation WHERE code = @code_reservation;

END

GO

-- ----------------------------
-- Procedure structure for clone_service
-- ----------------------------
DROP PROCEDURE [dbo].[clone_service]
GO
CREATE PROCEDURE [dbo].[clone_service]
	@service_id int,
	@newtime Datetime2
AS
--DECLARE c_timetable cursor for select * from timetable where service_id = @service_id;
--DECLARE c_wagon cursor for select * from wagon where service_id = @service_id;
--DECLARE c_seats cursor for select * from wagon_seat where wagon_service_id = @service_id;

BEGIN
	DECLARE @service_train_code NVARCHAR(10);
	DECLARE @service_type NVARCHAR(2);
	DECLARE @new_service_id INT;

	SELECT 
		@service_train_code = code,
		@service_type = [type]
	FROM train_service WHERE id = @service_id;

	IF @service_train_code = '' return;

	INSERT INTO train_service([type], code) VALUES (@service_type, @service_train_code);
	SET @new_service_id = @@IDENTITY;

	DECLARE @offset_seconds INT = DATEDIFF(second, (SELECT TOP 1 tt.arrival FROM timetable tt WHERE tt.service_id = @service_id ORDER BY tt.arrival ASC), @newtime);

	INSERT INTO timetable (arrival, departure, service_id, station_code)
		SELECT 
			(DATEADD(second, @offset_seconds, arrival)) as arrival, 
			(DATEADD(second, @offset_seconds, departure)) as departure, 
			@new_service_id as service_id,
			station_code
		FROM timetable WHERE service_id = @service_id;

	INSERT INTO wagon ( service_id, name, seats, [type], [order])
		SELECT @new_service_id as service_id, name, seats, [type], [order]
		FROM wagon WHERE service_id = @service_id;

	INSERT INTO wagon_seat (wagon_service_id, wagon_order, seat_number, [type])
		SELECT @new_service_id as wagon_service_id, wagon_order, seat_number, [type]
		FROM wagon_seat WHERE wagon_service_id = @service_id;

/*
	OPEN c_timetable;
	While 1=1
	begin
		if @@Fetch_Status != 0 break;
		FETCH NEXT FROM c_timetable;

		
	end
	CLOSE c_timetable;
	DEALLOCATE c_timetable;
*/

END

GO

-- ----------------------------
-- Procedure structure for create_reservation
-- ----------------------------
DROP PROCEDURE [dbo].[create_reservation]
GO
CREATE PROCEDURE [dbo].[create_reservation]
	@name nvarchar(100),
	@email nvarchar(200),
	@paid int,
	@code nvarchar(40) OUT 
AS
BEGIN 
	WHILE 1=1 
	BEGIN
		SET @code = SUBSTRING(REPLACE(newid(), '-', ''), 0, 10);
		IF (SELECT COUNT(code) FROM reservation WHERE code = @code) = 0 break;
	END

	INSERT INTO reservation (code, name, email, paid, created) VALUES (@code, @name, @email, @paid, GETDATE());

END
GO

-- ----------------------------
-- Procedure structure for service_extend
-- ----------------------------
DROP PROCEDURE [dbo].[service_extend]
GO
CREATE PROCEDURE [dbo].[service_extend]
	@service_id int,
	@size int,
	@type NVARCHAR(10),
	@name NVARCHAR(50) = ''
AS
BEGIN
	DECLARE @LAST_WAGON INT = (SELECT MAX([order]) FROM wagon);
	
	INSERT INTO wagon (service_id, [order], seats, [type], [name]) VALUES (@service_id, @LAST_WAGON+1, @size, @type, @name);

	DECLARE @i int = 0
	WHILE @i < @size BEGIN
		SET @i = @i + 1
		INSERT INTO wagon_seat (wagon_service_id, [wagon_order], seat_number) VALUES (@service_id, @LAST_WAGON+1, @i);
	END
END

GO

-- ----------------------------
-- Indexes structure for table engine_driver
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table engine_driver
-- ----------------------------
ALTER TABLE [dbo].[engine_driver] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table reservation
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table reservation
-- ----------------------------
ALTER TABLE [dbo].[reservation] ADD PRIMARY KEY ([code])
GO

-- ----------------------------
-- Indexes structure for table reservation_place
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table reservation_place
-- ----------------------------
ALTER TABLE [dbo].[reservation_place] ADD PRIMARY KEY ([seat_number], [wagon_order], [service_id])
GO

-- ----------------------------
-- Indexes structure for table station
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table station
-- ----------------------------
ALTER TABLE [dbo].[station] ADD PRIMARY KEY ([code])
GO

-- ----------------------------
-- Indexes structure for table timetable
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table timetable
-- ----------------------------
ALTER TABLE [dbo].[timetable] ADD PRIMARY KEY ([station_code], [service_id])
GO

-- ----------------------------
-- Checks structure for table timetable
-- ----------------------------
ALTER TABLE [dbo].[timetable] ADD CHECK (([departure] IS NOT NULL OR [arrival]<[departure]))
GO

-- ----------------------------
-- Indexes structure for table train
-- ----------------------------
CREATE UNIQUE INDEX [train__IDX] ON [dbo].[train]
([engine_driver_id] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO

-- ----------------------------
-- Primary Key structure for table train
-- ----------------------------
ALTER TABLE [dbo].[train] ADD PRIMARY KEY ([code])
GO

-- ----------------------------
-- Indexes structure for table train_service
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table train_service
-- ----------------------------
ALTER TABLE [dbo].[train_service] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table wagon
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table wagon
-- ----------------------------
ALTER TABLE [dbo].[wagon] ADD PRIMARY KEY ([order], [service_id])
GO

-- ----------------------------
-- Indexes structure for table wagon_seat
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table wagon_seat
-- ----------------------------
ALTER TABLE [dbo].[wagon_seat] ADD PRIMARY KEY ([seat_number], [wagon_order], [wagon_service_id])
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[reservation_place]
-- ----------------------------
ALTER TABLE [dbo].[reservation_place] ADD FOREIGN KEY ([reservation_code]) REFERENCES [dbo].[reservation] ([code]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[reservation_place] ADD FOREIGN KEY ([seat_number], [wagon_order], [service_id]) REFERENCES [dbo].[wagon_seat] ([seat_number], [wagon_order], [wagon_service_id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[timetable]
-- ----------------------------
ALTER TABLE [dbo].[timetable] ADD FOREIGN KEY ([service_id]) REFERENCES [dbo].[train_service] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[timetable] ADD FOREIGN KEY ([station_code]) REFERENCES [dbo].[station] ([code]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[train]
-- ----------------------------
ALTER TABLE [dbo].[train] ADD FOREIGN KEY ([engine_driver_id]) REFERENCES [dbo].[engine_driver] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[train_service]
-- ----------------------------
ALTER TABLE [dbo].[train_service] ADD FOREIGN KEY ([code]) REFERENCES [dbo].[train] ([code]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[wagon]
-- ----------------------------
ALTER TABLE [dbo].[wagon] ADD FOREIGN KEY ([service_id]) REFERENCES [dbo].[train_service] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[wagon_seat]
-- ----------------------------
ALTER TABLE [dbo].[wagon_seat] ADD FOREIGN KEY ([wagon_order], [wagon_service_id]) REFERENCES [dbo].[wagon] ([order], [service_id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
