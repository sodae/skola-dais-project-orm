﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Model;
using Model.Entity;
using Model.Entity.Service;
using Model.Repository;
using Model.Tests;

namespace dais_project
{
    class Program
    {
        static void Main(string[] args)
        {
            // unit tests before run
            MapTest.MappingTest();
            // continue if all test is ok.

            Connection connection = new Connection();
            connection.Open();

            TrainRepository trainRepository = new TrainRepository(connection);
            foreach (var enginer in trainRepository.FindEnginer())
            {
                Console.WriteLine("Inza: {0} ma vlak {1}", enginer.Name, enginer.Train != null ? enginer.Train.Code : "Žádný");
            }

            foreach (var train in trainRepository.FindTrain())
            {
                Console.WriteLine("Vlak: {0} ma ridice {1}", train.Title , train.EngineDriver.Name);
            }

            ServiceRepository serviceRepository = new ServiceRepository(connection);

            printService(serviceRepository.GetService(4));

            foreach (var service in serviceRepository.FindService())
            {
                printService(service);
            }

            // 7. 12. 2014 14:55:00
            foreach (var service in serviceRepository.FindServiceByStation("ostrava", "praha", false, new DateTime(2013, 12, 7, 14, 55, 0)))
            {
                printService(service.Service);
            }
            // 7. 12. 2014 14:55:00
            foreach (var service in serviceRepository.FindServiceByStation("ostrava", "praha", true, new DateTime(2014, 12, 7, 14, 55, 0)))
            {
                printService(service.Service);
            }

            connection.Close();

            Console.ReadKey();
        }

        private static void printService(Service service)
        {
            Console.WriteLine("Spoj {0}", service.Code);
            foreach (var timetable in service.Timetable)
            {
                Console.WriteLine("- {0}: {1} - {2}", timetable.Station.Name, timetable.Arrival, timetable.Departure);
            }
        }

    }


}
